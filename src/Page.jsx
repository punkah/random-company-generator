import React, { Component } from "react";
import styled from "styled-components";
import Bullshit from "./page/Bullshit";
import Footer from "./page/Footer";
import Header from "./page/Header";

const StyledPage = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

class Page extends Component {
  render() {
    return (
      <StyledPage>
        <Header />
        <Bullshit />
        <Footer />
      </StyledPage>
    );
  }
}

export default Page;
