import React from "react";
import styled from "styled-components";

const StyledText = styled.span`
  font-weight: 400;
  text-decoration: underline;
  text-decoration-color: aquamarine;
`;

const RandomText = ({ children }) => {
  return <StyledText>{children}</StyledText>;
};

export default RandomText;
