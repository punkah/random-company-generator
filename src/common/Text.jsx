import React from "react";
import styled from "styled-components";

const StyledText = styled.span`
  font-weight: 300;
`;

const Text = ({ children }) => {
  return <StyledText>{children}</StyledText>;
};

export default Text;
