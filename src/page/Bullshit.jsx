import React from "react";
import styled from "styled-components";
import Actions from "../common/Actions";
import Nouns from "../common/Nouns";
import Technologies from "../common/Technologies";
import RandomText from "./../common/RandomText";
import Text from "./../common/Text";

const StyledTextContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

Array.prototype.random = function() {
  return this[Math.floor(Math.random() * this.length)];
};

const Bullshit = () => {
  const tech = `${Technologies.random()}, ${Technologies.random()} and ${Technologies.random()}`;
  const goal = () => Actions.random().toLowerCase();
  const something = () => Nouns.random().toLowerCase();

  return (
    <StyledTextContainer className="timeline">
      <Text>
        {`We are a startup on a mission to disrupt  `}
        <RandomText>{something()}</RandomText>
        {". "}
        <br />
        {`We're changing the way `}
        <RandomText>{something()} </RandomText>
        <RandomText>{goal()}s</RandomText>
        {". "}
        <br />
        {"Our technology stack includes "}
        <RandomText>{tech}</RandomText>
        {". "}
        <br />
        {`Come work on the most interesting `}
        <RandomText>{something()}</RandomText>
        {" application in the universe. "}
        <br />
        {`We're making the world a better place by `}
        <RandomText>{goal()} </RandomText>
        <RandomText>{something()}s</RandomText>
        {" and aim to connect the world by applying innovative technology to "}
        <RandomText>{something()}s.</RandomText>
      </Text>
    </StyledTextContainer>
  );
};

export default Bullshit;
