import React from "react";
import styled from "styled-components";
import RandomText from "./../common/RandomText";
import Text from "./../common/Text";
import Words from "./../common/Words";

const StyledHeader = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

Array.prototype.random = function() {
  return this[Math.floor(Math.random() * this.length)];
};

const Header = () => {
  const name = Words.random();

  return (
    <StyledHeader>
      <Text>{"@ "}</Text>
      {name && (
        <RandomText>
          {name.substring(0, 1).toUpperCase()}
          {name.substring(1)} {"Limited"}
        </RandomText>
      )}
      {/* <div className="buttons">
        <button type="button">Refresh</button>
      </div> */}
    </StyledHeader>
  );
};

export default Header;
